        ;; IO routines

        ;; Initialise 6850
io_init:
        ld a, IO_6850_RESET     ; Master reset
        out (IO_6850_CONTROLPORT), a

        ;; 8 bits, 1 stop bit; 1:1 divide
        ;; Clock divide 64
        ;; Tx interrupt disabled
        ld a, IO_6860_8_BITS | IO_6850_1_STOP | IO_6850_64_DIV | IO_6850_TXI_DIS
        out (IO_6850_CONTROLPORT), a

        ret

        ;; Expects byte to send in A
        ;; Destroys: A
io_sendbyte:
        push    af
.waitforready:
        in      a, (IO_6850_CONTROLPORT)
        bit     IO_6850_TDRE, a
        jr      z, .waitforready
        pop     af
        out     (IO_6850_DATAPORT), a
        ret

io_send_string:
        ld      a, (hl)
        cp      a, 0
        jr      z, io_send_string_1
        call    io_sendbyte
        inc     hl
        jr      io_send_string
io_send_string_1:
        ret

        ;; Receive string
recv_string:
        ld      d, 0
        ld      hl, io_recv_buffer
recv_string_1:
        call    tmp_recv_byte
        ld      (hl), a
        inc     hl
        inc     d
        ld      e, a            ; Save a
        ld      a, d
        cp      io_recv_buffer_len - 1
        jr      z, recv_string_2 ; Run out of space
        ld      a, e             ; Restore a
        cp      a, $0a
        jr      nz, recv_string_1
recv_string_2:
        ;; Assumes we will have a CRLF
        dec     hl
        dec     hl
        dec     d
        dec     d
recv_string_3:
        ld      (hl), 0
        ld      a, d
        ld      (io_recv_buffer_cur), a
        ret

        ;; Receive a byte into A
recv_byte:
recv_waitforready:
	;; ld	de, 0xffff
	;; call	delay
        in      a, (IO_6850_CONTROLPORT)
	;; call    io_sendbyte
        bit     IO_6850_RDRF, a
        jr      z, recv_waitforready
        in      a, (IO_6850_DATAPORT)
        ret

        ;; Receive a byte into A
tmp_recv_byte:
.tmp_recv_waitforready:
        in      a, (IO_6850_CONTROLPORT)
	;; call    io_sendbyte
        bit     IO_6850_RDRF, a
        jr      z, recv_waitforready
        in      a, (IO_6850_DATAPORT)
        ;; Print it out
        push    af
        ld      hl, debug_buffer
        ld      (hl), a
        inc     hl
        ld      (hl), 0
        SEND_STR        debug_buffer
        pop     af
        ret
