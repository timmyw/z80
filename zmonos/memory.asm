        ;; Memory map

io_recv_buffer_len      = $ff

io_recv_buffer          = $8000
io_recv_buffer_cur      = $8100 ; 2 bytes?

mon_cur_pointer         = $8102 ; 2 bytes for current pointer
mon_str_buf             = $8104 ; 200 bytes

;; mon_command_buffer      = $81cb ; 200 bytes
str_split_count         = $81cb ; Number of tokens; 2 bytes
str_split_table         = $81cd ; Array of pointers for each token ;
                                ; 20x2 bytes
str_split_tokens        = $81f5 ; Buffer for tokens; 200 bytes

mon_dm_line_count       = $82bd ; Number of lines to dump; 1 byte
mon_echo_command        = $82be ; Boolean - echo full command; 1 byte

FREE1                   = $82bf ;
mon_work16_1            = $82c0 ; Working buffer
mon_work16_2            = $82c2
mon_work16_3            = $82c4

next_memory             = $82c6

debug_buffer            = $8f00 ; Debug work area; 255 bytes
