        ;; (Null-terminated) string utilities

        ;; Copy null-terminated string
        ;; Source in DE
        ;; Dest in HL
strcpy:
        ld      a, (de)
        ld      (hl), a
        cp      0
        jr      z, strcpy_done
        inc     hl
        inc     de
        jr      strcpy
strcpy_done:
        ret

        ;; Copy null-terminated string
        ;; Source in hl
        ;; Dest in de
strcpy_dehl:
        push    hl
        push    de
        pop     hl
        pop     de
        call    strcpy

        ;; Null-terminated string cmp
        ;; Compare the string in hl to the string in de.
        ;;
        ;; Destroys a, de
        ;; Preserves hl
strcmp:
        push    hl
        ld      a, (de)
        cp      (hl)
        pop     hl
        ret     nz
        cp      0
        ret     z
        inc     hl
        inc     de
        jr      strcmp

        ;; Split incoming string into space separated tokens
        ;; Works off the io_recv_buffer
split_command_string:
        ;; Copy into the token buffer
        ld      de, str_split_tokens
        ld      hl, io_recv_buffer
        call    strcpy_dehl
        ;; Get ready to scan through token string
        ld      hl, str_split_tokens
        ld      de, str_split_tokens ; Temp copy of token start
        ld      ix, str_split_table  ; Where we will store pointer
        ld      b, 0
        ld      c, b            ; Counter
scs_0:
        ;; Check for the end of the buffer
        ld      a, (hl)
        cp      0
        jr      z, scs_3        ; End of all tokens
        ;; Check for a space
        cp      $20
        jr      z, scs_2
        inc     hl
        jr      scs_0
scs_2:
        ;; We have a word delim
        inc     bc                    ; Inc token count
        ld      (hl), $00             ; Null terminate the current
                                      ; token
        ;; Store pointer to word and reset to start of new word
        ld      (ix), e
        inc     ix
        ld      (ix), d
        inc     ix
        inc     hl                    ; Move onto next word/nul
        push    hl                    ; Point DE to beginning of next
                                      ; word
        pop     de
        jr      scs_0
scs_3:
        ;; Capture final word
        inc     bc
        ld      (hl), $00
        ld      (str_split_count), bc
        ld      (ix), e
        inc     ix
        ld      (ix), d
        inc     ix
        ld      (ix), $00
        ret
