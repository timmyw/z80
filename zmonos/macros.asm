        ;; Send monitor string.
        macro   SEND_MON_STR
        ld      hl, mon_str_buf
        call    io_send_string
        endmacro

        macro   SEND_STR, mem16
        ld      hl, \mem16
        call    io_send_string
        endmacro

        ;; Load word in memory
        macro   LD_WORD, dest, word16
        ld      hl, \dest
        ld      (hl), \word16 & $ff
        inc     hl
        ld      (hl), (\word16 >> 8) & $ff
        endmacro

        ;; Load byte in memory
        macro   LD_BYTE, dest, byte8
        ld      hl, \dest
        ld      (hl), \byte8
        endmacro

        macro   LDREG_FRM_REG, dest, src
        push    \src
        pop     \dest
        endmacro

        macro   CHK_PARAM_COUNT, req, start
        ld      a, (str_split_count)
        cp      \req
        jr      z, \start
        ret     nz
        endmacro
