;;; monitor
;;;
;;;

        include	'constants.asm' ;
        include 'macros.asm'    ;
        include 'memory.asm'    ; Memory map

romsize         = 0x8000

        org 0x0000

        include 'reset.asm'     ; Reset vectors

zmonos_start:
        ld      sp, 0xffff      ; Initialise stack pointer
        call    io_init

        call    init_monitor
        ld      hl, welcome
        call    io_send_string

.command_loop:

        ld      hl, prompt
        call    io_send_string

        ;; Command loop

        ;; Check for echo
        call    recv_string
        ld      ix, mon_echo_command
        ld      a, (ix)
        cp      $0
        jr      z, .command_loop1
        SEND_STR io_recv_buffer
        SEND_STR crlf

.command_loop1:
        ld      hl, io_recv_buffer
        call    split_command_string
        call    handle_command

.command_loop0:
        jr      .command_loop

        ;; Command string in str_split_tokens
handle_command:
        ld      a, (str_split_count)
        cp      $0
        ret     z               ; If we have no command
        ld      hl, str_split_tokens
        ld      de, cmd_dump
        call    strcmp
        jr      nz, .hc_1
        call    dump_memory
        ret
.hc_1:
        ld      de, cmd_poke
        call    strcmp
        jr      nz, .hc_2
        call    cmd_poke_memory
        ret

.hc_2:
        ld      de, cmd_jump
        call    strcmp
        jr      nz, .hc_3
        call    cmd_jump_to_address
        ret

.hc_3:
.hc_unknown:
        ld      hl, mon_str_buf
        ld      de, cmd_unknown
        call    strcpy
        ld      de, space
        call    strcpy
        ld      de, msg_open_sq
        call    strcpy
        ld      de, str_split_tokens ; Show what we received
        call    strcpy
        ld      de, msg_close_sq
        call    strcpy
        ld      de, crlf
        call    strcpy
        ld      hl, mon_str_buf
        call    io_send_string
        ret

        ;; Initialise monitor config and data
init_monitor:

        LD_WORD mon_cur_pointer, $8000
        LD_BYTE mon_dm_line_count, $05
        LD_BYTE mon_echo_command, $01

        ei                      ; Enable interrupts when ready

        ret

        ;; Includes

        include	'io.asm'
	include 'utils.asm'
        include 'string.asm'
        include 'messages.asm'
        include 'commands.asm'
        include 'config.asm'

end:
	ds romsize-end,255
