
        ;; Command names
cmd_dump        defb    'dm', 0
cmd_poke        defb    'poke', 0
cmd_jump        defb    'jump', 0
cmd_config      defb    'config', 0

        ;; Retrieve a token pointer
        ;; Index in a
        ;; Returns token pointer in de
        ;; Preserves hl
get_token_by_idx:
        push    hl
        ld      hl, str_split_table
.get_token_by_idx_0:
        ld      e, (hl)
        inc     hl
        ld      d, (hl)
        cp      $00             ; Got the right token pointer?
        jr      z, .get_token_by_idx_1
        inc     hl
        dec     a
        jr      .get_token_by_idx_0
.get_token_by_idx_1:
        pop     hl
        ret

        ;; Expect memory and bytes as parameters
        ;; poke XXXX YYyyYYyy...
cmd_poke_memory:
        CHK_PARAM_COUNT $03, cmd_pm_start

cmd_pm_start:
        ;; Load destination from hex string (param 1)
        ld      a, $1
        call    get_token_by_idx
        call    hex_string_to_word ; in hl
        ld      (mon_work16_1), hl ; store it - working count
        ld      (mon_work16_2), hl ; store it for setting current
                                   ; pointer later

        ;; Byte string.  This can be any number (up to 200-4) chars of
        ;; hex (hopefully in 2s)
        ld      a, $2
        call    get_token_by_idx
        LDREG_FRM_REG iy, de

.cmd_pm_start0
        ld      a, (iy)           ; First hex char/nibble
        cp      $0
        jr      z, .cmd_pm_start1 ; We're done

        ;; This needs DE
        LDREG_FRM_REG   de, iy
        call    hex_string_to_byte

        ;; Poke byte
        ld      ix, (mon_work16_1)
        ld      (ix), a

        inc     ix
        ld      (mon_work16_1), ix

        inc     iy              ; Jump to next 2 char hex byte
        inc     iy
        jr      .cmd_pm_start0

.cmd_pm_start1

        ;; Switch cur pointer to new poke start
        ld      ix, mon_cur_pointer
        ld      hl, (mon_work16_2)
        ld      (ix), l
        ld      (ix+1), h

        ret

dump_memory:
        ;; First check if we have a memory address
        ld      a, (str_split_count)
        cp      $01
        jr      z, dm_start

dm_init:
        ld      hl, mon_str_buf
        ld      a, $1           ; Second token
        call    get_token_by_idx
        call    hex_string_to_word

        ld      ix, mon_cur_pointer
        ld      (ix), l
        ld      (ix+1), h

dm_start:
        ld      bc, (mon_dm_line_count)
        ld      b, c

dm_loop:
        push    bc              ; Store line count for later
        ld      hl, mon_str_buf
        ld      de, (mon_cur_pointer) ; Dump out current
        call    convert_to_hex
        ld      de, space
        call    strcpy

        ld      b, $10                ; 16 bytes at a time
        ld      ix, (mon_cur_pointer) ; And what is being pointed to

dm_0:
        ld      d, (ix)
        inc     ix
        call    convert_byte_to_hex
        ld      de, space
        call    strcpy
        djnz    dm_0

        ;; ASCII chars
        ld      b, $10
        ld      ix, (mon_cur_pointer)
dm_1:
        ld      a, (ix)         ; Load byte
        call    ascii_to_char
        ld      (hl), a         ; Char to output
        inc     hl
        inc     ix
        djnz    dm_1
        ld      (hl), 0

        ld      de, crlf
        call    strcpy

        SEND_MON_STR

        ld      (mon_cur_pointer), ix ; Increment current pointer

        ;; Loop through lines
        pop     bc              ; Check line count
        djnz    dm_loop

        ret

loop

        ;; Jump to the hex address specified and execute from there
cmd_jump_to_address:
        ;; Get destination
        ld      a, $1
        call    get_token_by_idx
        call    hex_string_to_word ; Returns in hl

        push    hl              ; Jump to contents of hl but need to
                                ; look after SP
        ret
