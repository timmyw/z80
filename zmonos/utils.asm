
        ;; Delay loop
        ;; Expects delay start count in de
delay:
delay_loop1:
        dec 	de
        ld 	a, d
        or 	e
        jr 	nz, delay_loop1
        ret

        ;; nibble in lower half of A
nibble_to_hex:
        and     a, $0f          ; Mask out top portion
        cp      a, $0a
        jp      p, nibble_bigger
        ;; 0-9
        add     a, '0'
        ret
nibble_bigger:
        ;; A-F
        add     a, 'A'-10
        ret

        ;; D points to input
        ;; HL points to output
convert_byte_to_hex:
        ld      a, d
        ccf
        sra     a
        sra     a
        sra     a
        sra     a
        call    nibble_to_hex
        ld      (hl), a
        inc     hl
        ld      a, d
        call    nibble_to_hex
        ld      (hl), a
        inc     hl

        ld      (hl), 0
        ret

        ;; DE points to input
        ;; HL points to output
convert_to_hex:
        ;; ld      a, d            ; Start with high order byte
        call    convert_byte_to_hex
        ld      d, e            ; Move low order byte to d
        call    convert_byte_to_hex

        ret

        ;; Hex to binary.  Converts 4 char hex string into binary
        ;; In: de points to source string
        ;; Out: hl contains word
hex_string_to_word:
        call    hex_string_to_byte
        ld      h, a
        inc     de
        call    hex_string_to_byte
        ld      l, a
        ret

        ;; Hex to binary.  Converts 2 char hex string into binary
        ;; de points to source string
hex_string_to_byte:
        ld      a, (de)
        call    char_to_nibble  ; a -> c
        ld      b, c            ; store c
        inc     de
        ld      a, (de)         ; Next nibble
        call    char_to_nibble  ; into c
        ld      a, b
        ccf
        rlc     a
        rlc     a
        rlc     a
        rlc     a
        or      c
        ret                     ; With byte in a

        ;; Convert char in a to nibble in c
char_to_nibble:
        cp      '0'
        ret     c               ; Smaller than '0'
        cp      '9'+1
        jr      nc, c2n_1
        ccf
        sub     '0'
        ld      c, a

c2n_1:
        ;; Make lower case
        or      %100000

        ;; a-f -
        cp      'a'
        ret     c               ; Smaller than 'a'
        cp      'f'+1
        ret     nc              ; Bigger than 'f'

        sub     'a'- 10
        ld      c, a
        ret

        ;; Convert a to a displayable character
ascii_to_char:
        cp      $20
        jr      c, a2c_0
        cp      $7f
        ret     c
a2c_0:
        ld      a, '.'
        ret
