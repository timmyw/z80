        ;; Constants

        ;; IO constants

IO_6850_CONTROLPORT     = 0x80
IO_6850_DATAPORT        = 0x81

        ;; 6850

        ;; Bit schecks
IO_6850_RDRF            = 0x0   ; Data read ready
IO_6850_TDRE            = 0x1   ; Data transmit ready

        ;; Reset command
IO_6850_RESET           = %00000011

IO_6850_WORD_SELECT     = %00010100
;; IO_GS_SETTINGS          = %10010110

IO_6850_64_DIV          = %00000010
IO_6850_1_STOP          = %00000100
IO_6860_8_BITS          = %00010000
IO_6850_TXI_DIS         = %10000000
