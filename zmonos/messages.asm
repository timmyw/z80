        ;; Messages

welcome:        defb    10, 13, 'ZMONOS 0.1.6 2023 (c) tim@zipt.co', 13, 10, 0
prompt:         defb    '> ', 0

space:          defb    ' ', 0
dashes:         defb    '---', 0
arrow:          defb    '->', 0
crlf:           defb    13, 10, 0

msg_open_sq     defb    '[', 0
msg_close_sq    defb    ']', 0

cmd_unknown     defb    'Unknown command', 0
