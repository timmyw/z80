;;; Stage3
;;;

	include	'data.asm'

romsize:	equ 0x8000

outport:	equ 0x01

	org 0x0000

start:
        call init

start1:
        ld a, (outbyte1)
	out (outport), a
	call delay
	ld a, (outbyte2)
	out (outport), a
	call delay
	jp start1

delay:
	ld de, (delaycnt)
loop1:
	dec de
	ld a, d
	or e
	jp nz, loop1
	ret

init:
        ;; Copy our working data into place in RAM - this is more to
        ;; double check RAM than for any other reason.
	ld de, _delaycnt
	ld (delaycnt), de
        ld a, _outbyte1
        ld (outbyte1), a
        ld a, _outbyte2
        ld (outbyte2), a

	ret

        ;; Constants
_outbyte1:	equ	%01010101
_outbyte2:	equ	%10101010
_delaycnt:	equ	0x07ff

        ;; Data table
delaycnt:	equ	0xf000
outbyte1:       equ     0xf002
outbyte2:       equ     0xf003

end:
	ds romsize-end,255
