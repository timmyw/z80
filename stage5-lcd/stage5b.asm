z;;; Stage5a
;;;
;;; Initialise the LCD and write a few chars to it
;;;

romsize         = 0x8000

controlport     = 0xa0
outport         = 0xa1

	org 0x0000

start:

.wait_for_lcd_0:
        in      a, (controlport)
        bit     7, a
        jr      nz, .wait_for_lcd_0

	ld      a, %00111000         ; 0x38 = 8 bit; 2 line; 5x8 font
        out     (controlport), a

        ld      a, %00001111         ; 0x0f = Display on and cursor on; blinking
        out     (controlport), a

        ld      a, %00000001         ; 0x01 = Clear display
        out     (controlport), a

        ld      a, %00000110         ; 0x06 = Cursor shift to right
        out     (controlport), a

        ld      a, 'H'
        out     (outport), a

        ld      a, 'e'
        out     (outport), a

start_:
	jp      start_

end:
	ds romsize-end,255
