z;;; Stage5a
;;;
;;; Initialise the LCD and write a few chars to it
;;;

romsize         = 0x8000

controlport     = 0xa0
outport         = 0xa1

	org 0x0000

start:
	ld      a, %00111000         ; 0x38 = 8 bit; 2 line; 5x8 font
        call    send_lcd_control

        ld      a, %00001111         ; 0x0f = Display on and cursor on; blinking
        call    send_lcd_control

        ld      a, %00000001         ; 0x01 = Clear display
        call    send_lcd_control

        ld      a, %00000110         ; 0x06 = Cursor shift to right
        call    send_lcd_control

        call    wait_for_lcd
        ld      a, 'H'
        out     (outport), a

        call    wait_for_lcd
        ld      a, 'e'
        out     (outport), a

start_:
	jp      start_

send_lcd_control:
        push    af
.send_lcd_control_0:
        in      a, (controlport)
        bit     7, a
        jr      nz, .send_lcd_control_0
        pop     af
        out     (controlport), a
        ret

wait_for_lcd:
        in      a, (controlport)
        bit     7, a
        jr      nz, wait_for_lcd
        ret

end:
	ds romsize-end,255
