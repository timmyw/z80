#+TITLE: Z80
#+OPTIONS: toc:nil author:nil date:nil

* CPU

** Pinout

   [[./images/Z80_pinout.png]]

  |------+----+----+--------|
  | A11  |  1 | 40 | A10    |
  | A12  |  2 | 39 | A9     |
  | A13  |  3 | 38 | A8     |
  | A14  |  4 | 37 | A7     |
  | A15  |  5 | 36 | A6     |
  | CLK  |  6 | 35 | A5     |
  | D4   |  7 | 34 | A4     |
  | D3   |  8 | 33 | A3     |
  | D5   |  9 | 32 | A2     |
  | D6   | 10 | 31 | A1     |
  | +5V  | 11 | 30 | A0     |
  | D2   | 12 | 29 | GND    |
  | D7   | 13 | 28 | RFSH   |
  | D0   | 14 | 27 | M1     |
  | D1   | 15 | 26 | RESET  |
  | INT  | 16 | 25 | BUSRQ  |
  | NMI  | 17 | 24 | WAIT   |
  | HALT | 18 | 23 | BUSACK |
  | MREQ | 19 | 22 | WR     |
  | IORQ | 20 | 21 | RD     |
  |------+----+----+--------|

* Freerun

#+begin_quote
During the last half of the Instruction Opcode Fetch a Refresh address
is placed on the lower 7 bits of the Address bus. This can make the
upper 9 bits appear as if they are oscillating in respect to the clock
since they are driven low during that portion of the cycle. If you
only take the state of the address bus when /M1 is active, you will
see the appropriate memory address is on the bus.
#+end_quote

[[file:z80test0.gif]]


* Memory map

** Control logic

*** Control signals

    ~/MEMRD~ and ~/MEMWR~ need to be separate to match 8080 schema (6800 uses a combined RW signal).

    Notes:

    + If ~/MEMRQ~ is high, then both ~/MEMRD~ and ~/MEMWR~ will be high (off).
    + If ~/WR~ and ~/RD~ are low at the same time, then ~/MEMRD~ and ~/MEMWR~ are both going to
    assert (go low).

    |-------+-----+-----+---------------------+---------------------|
    | /MREQ | /RD | /WR | /MEMRD:/RW v /MEMRD | /MEMWR:/WR v /MEMRQ |
    |     0 |   0 |   0 |                   0 |                   0 |
    |     0 |   0 |   1 |                   0 |                   1 |
    |     0 |   1 |   0 |                   1 |                   0 |
    |     0 |   1 |   1 |                   1 |                   1 |
    |     1 |   X |   X |                   1 |                   1 |
    |       |     |     |                     |                     |

*** Address logic
|-----+-----+-----+-------------+---------+---------+---------|
| A15 | A14 | A13 | X:A13 v A14 | X v A15 | ~ROM:CE | RAM:CS2 |
|-----+-----+-----+-------------+---------+---------+---------|
|   0 |   0 |   0 |           0 |       0 | ON      | OFF     |
|   0 |   0 |   1 |           1 |       1 | OFF     | ON      |
|   0 |   1 |   0 |           1 |       1 | OFF     | ON      |
|   0 |   1 |   1 |           1 |       1 | OFF     | ON      |
|   1 |   0 |   0 |           0 |       1 | OFF     | ON      |
|   1 |   0 |   1 |           1 |       1 | OFF     | ON      |
|   1 |   1 |   0 |           1 |       1 | OFF     | ON      |
|   1 |   1 |   1 |           1 |       1 | OFF     | ON      |
|     |     |     |             |         |         |         |


+ 8K ROM starting at ~$0000~ and running through to ~$0fff~.
+ 56K RAM starting at ~$1000~ through to ~$ffff~.

* IO v2

  Uses a 74HC138 to provide Enable type signals for 8 IO devices, using address
  lines A7-A5.

  | Input  | Pin | Connect |                    |
  |--------+-----+---------+--------------------|
  | A0/1/A |   1 | A5      |                    |
  | A1/2/B |   2 | A6      |                    |
  | A2/3/C |   3 | A7      |                    |
  | /E1    |   4 | A4      | Keep low to enable |
  | /E2    |   5 | /IOREQ  |                    |
  | E3     |   6 | /M1     |                    |
  |--------+-----+---------+--------------------|

  *Current assignment*
  | Address    | E  |  Hex | Device |
  |------------+----+------+--------|
  | 100 0 000x | Y4 | 0x80 | 6850   |
  | 101 0 000x | Y5 | 0xA0 | LCD    |

* IO v1

  /IORQ goes low
  /RD or /WR goes low

  6850 disabled when /M1 goes low - avoid the problem of /IORQ going low during
  interrupt acknowledge

** IO addresses

   Z80 provides 8 bits of addressable IO.  This board follows Grant Searles IO
   layout so $80 is used by the 6850 for the UART/serial.

   |  Address | Hex | Usage | Details    |
   |----------+-----+-------+------------|
   | 76543210 |     |       |            |
   |----------+-----+-------+------------|
   | 10xxxxxx |     | 6850  |            |
   | 10000000 | $80 |       | IO control |
   | 10000001 | $81 |       | IO data    |
   | 01000000 |     | LCD   |            |
   | 01000001 |     |       |            |


* Utilities

** Generate addresses

#+begin_src haskell
  import Text.Printf

  line :: Int -> String
  line x = printf "0x%04x %016b" x x

  main :: IO ()
  main =
    mapM_ (\x -> putStrLn (line (2^x))) (reverse $ take 16 [0..])
#+end_src

#+results:
#+begin_example
0x8000 1000000000000000
0x4000 0100000000000000
0x2000 0010000000000000
0x1000 0001000000000000
0x0800 0000100000000000
0x0400 0000010000000000
0x0200 0000001000000000
0x0100 0000000100000000
0x0080 0000000010000000
0x0040 0000000001000000
0x0020 0000000000100000
0x0010 0000000000010000
0x0008 0000000000001000
0x0004 0000000000000100
0x0002 0000000000000010
0x0001 0000000000000001
#+end_example
