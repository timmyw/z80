;;; Stage2a
;;;

romsize = 0x8000
outport = 0x01

	org 0x0000

start:
	ld a, 0x55
	out (outport), a
	jp start

end:
	ds romsize-end,255
