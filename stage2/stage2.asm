;;; Stage2
;;;
;;; Assume all IO memory addresses are valid targets
;;; Write to a port
;;; Loop to delay
;;; Write a different value
;;; Delay
;;; Loop for ever

romsize = 0x8000

outport = 0x01

	org 0x0000

start:
	ld a, 0x55
	out (outport), a

	ld de, 0xffff
delay1:
	ld bc, 0xffff
delay1_a:
	ld hl, 0xffff
delay1_b:
	dec hl
	jr nz, delay1_b
	dec bc
	jr nz, delay1_a
	dec de
	jr nz, delay1

step2:
	ld a, 0xaa
	out (outport), a

	ld de, 0xffff
delay2:
	ld bc, 0xffff
delay2_a:
	ld hl, 0xffff
delay2_b:
	dec hl
	jr nz, delay2_b
	dec bc
	jr nz, delay2_a
	dec de
	jr nz, delay2

	jp start

end:
	ds romsize-end,255
