;;; Stage4a
;;;
;;; Uses the 6850 ACIA/UART chip
;;;
;;; Tests IO by initialising the chip and then sending test data
;;;

        include	'data.asm'

romsize:	equ 0x8000

        org 0x0000

start:
        ld      sp, 0xffff      ; Initialise stack pointer
        call    init

start1:
        ld      a, outbyte1
        out     (IO_6850_DATAPORT), a
        ;; call sendbyte
        call    delay
        ld      a, outbyte2
        out     (IO_6850_DATAPORT), a
        ;; call sendbyte
        call    delay
        jr      start1

delay:
        ld de, delaycnt
loop1:
        dec de
        ld a, d
        or e
        jp nz, loop1
        ret

        ;; Expects byte to send in A
        ;; Destroys: A
sendbyte:
        push    af
waitforready:
        in      a, (IO_6850_CONTROLPORT)
        bit     IO_6850_TDRE, a
        jr      z, waitforready
        pop     af
        out     (IO_6850_DATAPORT), a
        ret
init:

        ;; Initialise the 6850
        ld a, IO_6850_RESET    ; Master reset
        out (IO_6850_CONTROLPORT), a

        ld a, IO_6850_WORD_SELECT ; 8 bits, 1 stop bit; 1:1 divide
        out (IO_6850_CONTROLPORT), a

        ld      a, 096H
        out     ($80), a

        ret

        ;; Constants
outbyte1:	equ	%01010101
outbyte2:	equ	%10101010
delaycnt:	equ	0x03ff

end:
	ds romsize-end,255
