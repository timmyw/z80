;;; Stage4a
;;;
;;; Uses the 6850 ACIA/UART chip
;;;
;;; Tests IO by initialising the chip and then sending test data
;;;

        include	'data2.asm'

romsize         = 0x8000

        org 0x0000

start:
        ld      sp, 0xffff      ; Initialise stack pointer
        call    init

        ld      hl, welcome
        call    send_string
start1:
        jr      start1

delay:
        ld de, delaycnt
loop1:
        dec de
        ld a, d
        or e
        jp nz, loop1
        ret

        ;; Expects byte to send in A
        ;; Destroys: A
sendbyte:
        push    af
waitforready:
        in      a, (IO_6850_CONTROLPORT)
        bit     IO_6850_TDRE, a
        jr      z, waitforready
        pop     af
        out     (IO_6850_DATAPORT), a
        ret
init:

        ;; Initialise the 6850
        ld a, IO_6850_RESET    ; Master reset
        out (IO_6850_CONTROLPORT), a

        ld a, IO_6850_WORD_SELECT ; 8 bits, 1 stop bit; 1:1 divide
        out (IO_6850_CONTROLPORT), a

        ld      a, 096H
        out     ($80), a

        ret

send_string:
        ld      a, (hl)
        cp      a, 0
        jr      z, send_string_done
        call    sendbyte
        inc     hl
        jr      send_string
send_string_done:
        ret

        ;; Constants
outbyte1        = %01010101
outbyte2	= %10101010
delaycnt	= 0x03ff

welcome:        defb    'ZMON 0.1 2023', 13, 10, 0

end:
	ds romsize-end,255
