;;
;;

romsize = 0x8000

	org 	0x0000

start:
	nop
	nop
	nop
	nop			; Give us a chance to check the A0..A3 lines
	jr start

end:
	ds romsize-end,255	; 8000h+RomSize-End if org 8000h
