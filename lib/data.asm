	;; Data table for monitor
	;;


        ;; Data table for stages

        ;; IO constants

IO_6850_CONTROLPORT:    equ     0x80
IO_6850_DATAPORT:       equ     0x81

        ;; 6850

IO_6850_TDRE:   equ     0x1
IO_6850_RESET:  equ     %00000011

IO_6850_WORD_SELECT:    equ     %00010100

IO_6860_8_BITS: equ     %00010000
IO_6850_1_STOP: equ     %00000100
