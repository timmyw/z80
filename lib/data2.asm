	;; Data table for monitor
	;;


        ;; Data table for stages

        ;; IO constants

IO_6850_CONTROLPORT =  0x80
IO_6850_DATAPORT    =  0x81

        ;; 6850

IO_6850_TDRE            = 0x1
IO_6850_RESET           = %00000011

IO_6850_WORD_SELECT     = %00010100

IO_6860_8_BITS          = %00010000
IO_6850_1_STOP          = %00000100
