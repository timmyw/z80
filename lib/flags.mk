
.PHONY: program

# Assembler
# ASM=z80asm
# ASMFLAGS=-I ../lib

ASM=vasmz80_oldstyle
ASMFLAGS=-Fbin -I../lib

# Binary to hex
BIN2HEX=bin2hex

# Programmer
PROGRAMMER=minipro
EEPROM=AT28C256
PROGFLAGS=-p ${EEPROM} -w

program: all
	@if [ "$(IMG)" = "" ]; then \
		echo "binary missing (make program IMG=binary)"; \
	else \
		echo $(PROGRAMMER) $(PROGFLAGS) $(IMG); \
		$(PROGRAMMER) $(PROGFLAGS) $(IMG); \
	fi

clean:
	rm -f *.bin *.lst *.hex
