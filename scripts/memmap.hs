import Text.Printf

line :: Int -> String
line x = printf "0x%04x %016b" x x
main :: IO ()
main = 
  mapM_ (\x -> putStrLn (line (2^x))) (reverse $ take 16 [0..])
