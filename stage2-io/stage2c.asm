;;; Stage2
;;;
;;; Write to an output port (0x81)
;;; Loop forever

romsize:	equ 0x8000

outport:	equ 0x81
ctrlport:       equ 0x80

	org 0x0000

start:
	ld a, %01010101
	out (outport), a
	ld a, %10101010
	out (ctrlport), a

	jp start

end:
	ds romsize-end,255
