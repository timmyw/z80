;;; Stage2a
;;; 

romsize:	equ 0x8000

outport:	equ 0x01
	
	org 0x0000

start:
	ld a, 0x55
start0:	
	out (outport), a
	jp start0
	
end:
	ds romsize-end,255
