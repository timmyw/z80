;;; Stage2a
;;;

;;; Loop
;;; no-loop :
;;; 0x01ff  : 1.66 ms
;;; 0x03ff  : 3.3  ms
;;; 0x07ff  : 6.6  ms

romsize:	equ 0x8000

outport:	equ 0x01

	org 0x0000

start:
	ld a, %01010101
	out (outport), a

	ld de, 0x01ff
loop1:
	dec de
	ld a, d
	or e
	jp nz, loop1

	ld a, %10101010
	out (outport), a

	ld de, 0x01ff
loop2:
	dec de
	ld a, d
	or e
	jp nz, loop2

	jp start

end:
	ds romsize-end,255
