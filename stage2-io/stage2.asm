;;; Stage2
;;;
;;; Assume all IO memory addresses are valid targets
;;; Write to a port
;;; Loop to delay
;;; Write a different value
;;; Delay
;;; Loop for ever

romsize:	equ 0x8000
delay_count:	equ 0xff

outport:	equ 0x81

	org 0x0000

start:
	ld a, 0x55
	out (outport), a

	ld de, 0x03ff
loop1:
	dec de
	ld a, d
	or e
	jp nz, loop1

step2:
	ld a, 0xaa
	out (outport), a

	ld de, 0x03ff
loop2:
	dec de
	ld a, d
	or e
	jp nz, loop2

	jp start

end:
	ds romsize-end,255
